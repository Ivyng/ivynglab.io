module.exports = {
  workexp: [
    {
      time: "Mar 2018",
      title: "Freelance Logo & T-shirt Design for Team SWFIT",
      achievements: [
        "Used Adobe Illustrator to create logo and T-shirt design."
      ]
    },
    {
      time: "July 2018",
      title: "Freelance Logo design for Oarsman",
      achievements: ["Utilized positive and negative space design techniques."]
    },
    {
      time: "Aug 2018",
      title: "Collaboration with Mount Mirium",
      achievements: [
        "Participated in Mount Mirium magazine design process.",
        "Learned techniques for creating beautiful magazines for presentation."
      ]
    },
    {
      time: "Dec 17 2018 - Mar 15 2019",
      title: "Intern Graphic Designeer @ ROD Creative Sdn Bhd",
      achievements: [
        "Communicated with the customer to ensure that their expectations are properly met with the designs.",
        "Experienced printing T-shirt, photo books, packaging, name card, and tote bag."
      ]
    },
    {
      time: "Sept 2018",
      title: "Freelance Logo & Name Card for Inspiration Orient Group Sdn Bhd",
      achievements: [
        "Researched on logo design, and created a brand new logo using the knowledge.",
        "Designed a standard name card."
      ]
    },
    {
      time: "Oct 1 2019 - Nov 31 2019",
      title: "Barista @ DoubleTap Cafe",
      achievements: [
        "Learned latte art techniques in a week.",
        "Learned to communicate to customers in Hokkien and Japanese."
      ]
    },
    {
      time: "March 6 2020 - March 11 2020",
      title: "Freelance Personal Logo & Name Card for Mr Chew",
      achievements: [
        "Design a line art logo using the client's chinese name.",
        "Created an unorthodoxly designed name card."
      ]
    },
    {
      time: "Jan 1 2020 - April 1 2020",
      title: "Graphic Designer @ Media Art Advertising",
      achievements: [
        "Designed company product catalog.",
        "Learned checking client's artwork for suitablility for printing."
      ]
    }
  ],
  education: [
    {
      time: "2011-2015",
      school: "Secondary in Treacher Methoist Girl School",
      certification: "Sijil Pelaajaran Malaysia (SPM)"
    },
    {
      time: "2016-2019",
      school: "Diploma in Equator College",
      certification: "Major in Graphic and Multimedia"
    }
  ],
  software: [
    {
      name: "Photoshop",
      score: 7
    },
    {
      name: "Illustrator",
      score: 7
    },
    {
      name: "InDesign",
      score: 5
    },
    {
      name: "After Effect",
      score: 4
    },
    {
      name: "Premiere Pro",
      score: 4
    },
    {
      name: "Autodesk Maya",
      score: 6
    }
  ],
  skill: [
    {
      name: "Vector",
      score: 7
    },
    {
      name: "Layout",
      score: 7
    },
    {
      name: "Digital Drawing",
      score: 5
    },
    {
      name: "Wire Framing",
      score: 4
    },
    {
      name: "UI",
      score: 4
    },
    {
      name: "Latte Art",
      score: 3
    }
  ]
};
