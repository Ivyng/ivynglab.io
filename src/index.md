---
layout: index.njk
title: Home
---

# Hi

### I am Ivy, a Graphic Designer<br />located in Malaysia

Looking for a freelancer or full-time employee in Graphic Designing?  
Please feel free to contact me!

My designs are high-quality and affordable. I am experienced in  
designing flyers, brochures, business cards and more.  
Go ahead and take a look at my portfolio!

