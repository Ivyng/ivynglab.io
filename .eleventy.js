const fg = require('fast-glob');
const pluginSass = require("eleventy-plugin-sass");

const portfolioImages = fg.sync(['src/assets/img/portfolio/**/*']);
const videos = fg.sync(['src/assets/video/**/*']);

module.exports = function(config) {
  config.setTemplateFormats([
    "md",
    "njk",
    "png",
    "jpeg",
    "jpg",
    "gif",
    "woff",
    "woff2"
  ]);
  
  config.addCollection('portfolio', function(collection) {
    return portfolioImages.map(s => s.replace('src/', '/'));
  });

  config.addCollection('videos', function(collection) {
    return videos.map(s => s.replace('src/', '/'));
  })

  config.addPlugin(pluginSass, {
    watch: ["src/**/*.{scss,sass}", "!node_modules/**"],
    sourcemaps: true
  });

  return {
    templateFormats: ["html", "md", "njk"],
    dir: {
      input: "src",
      output: "_site",
      includes: "_includes",
      layouts: "_layouts"
    }
  };
};
